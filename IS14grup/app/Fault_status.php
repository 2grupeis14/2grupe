<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fault_status extends Model
{
    protected $fillable = [
        'status_id','fault_id'
    ];

}

<?php

namespace App\Http\Controllers;
use DB;
use auth;
use Illuminate\Http\Request;

class IssueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data = DB::table('issue')

            ->select('faults.id', 'faults.text','users.surname')
            ->leftJoin('users', 'users.id', '=', 'faults.user_id')
            ->where('faults.user_id', Auth::user()->id)
            ->orderBy('faults.created_at', 'desc');

        $search = $request->input('search');
        if ($search != '')
        {
            $pattern = '%' . $search . '%';
            $data->where('id', 'LIKE', $pattern);
            $data->orWhere('width', 'LIKE', $pattern);
            $data->orWhere('height', 'LIKE', $pattern);
            $data->orWhere('faults.created_at', 'LIKE', $pattern);
            $data->orWhere('faults.text', 'LIKE', $pattern);
        }

        //dd(Fault::with('fault_status')->get());
        //dd(Fault_status::groupBy('fault_id')->toSql());
        //dd(Fault_status::orderby('created_at','desc')->groupBy('fault_id')->get());

//        $data = DB::table("items")
//            ->select("items.*","items_count.price_group","items_count.quantity_group")
//            ->join(DB::raw("(SELECT
//              items_count.id_item,
//              GROUP_CONCAT(items_count.price) as price_group,
//              GROUP_CONCAT(items_count.quantity) as quantity_group
//              FROM items_count
//              GROUP BY items_count.id_item
//              ) as items_count"),function($join){
//                $join->on("items_count.id_item","=","items.id");
//            })
//            ->groupBy("items.id")
//            ->get();
//        print_r($data);


        /*

        $data = $data->paginate(5);*/

        dd($data);

        return view('/')->with(compact('data', 'search'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

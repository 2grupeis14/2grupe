<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;

class MainController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
}
class AdministratorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $data = DB::table('faults')
            ->select('faults.id', 'faults.text', 'users.surname')
            ->leftJoin('users', 'users.id', '=', 'faults.user_id')
            ->where('faults.user_id', Auth::user()->id)
            ->orderBy('faults.created_at', 'desc');

        $search = $request->input('search');
        if ($search != '') {
            $pattern = '%' . $search . '%';
            $data->where('id', 'LIKE', $pattern);
            $data->orWhere('width', 'LIKE', $pattern);
            $data->orWhere('height', 'LIKE', $pattern);
            $data->orWhere('faults.created_at', 'LIKE', $pattern);
            $data->orWhere('faults.text', 'LIKE', $pattern);
        }

        $pattern = "'%%'";

        $data = DB::select(DB::raw(sprintf('
            SELECT faults.*, IF(tmp.occupied, 1, 0) as occupied, tmp.status_id, statuses.name FROM faults
            LEFT JOIN 
            (
                SELECT t1.*, t1.created_at = t1.updated_at as occupied 
                FROM fault_statuses t1 LEFT JOIN fault_statuses t2 ON (t1.fault_id = t2.fault_id AND t1.updated_at < t2.updated_at)
                WHERE t2.updated_at IS NULL
            ) tmp 
            ON tmp.fault_id = faults.id
            LEFT JOIN statuses ON statuses.id = tmp.status_id
            WHERE faults.text LIKE %s OR faults.created_at LIKE %s             
            ORDER BY faults.created_at', $pattern, $pattern)));

        dd($data);

        return view('/administrator')->with(compact('data', 'search'));

    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}


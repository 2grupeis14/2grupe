<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Auth;

use App\comments;
use App\users;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $issue_id)
    {
        $data = DB::table('comments')
            ->select('comments.id', 'comments.text', 'users.name', 'comments.created_at')
            ->leftJoin('users', 'users.id', '=', 'comments.user_id')
            ->where('comments.fault_id', $issue_id)
            ->orderBy('comments.created_at', 'desc');

        $search = $request->input('search');
        if ($search != '') {
            $pattern = '%' . $search . '%';
            $data->where('id', 'LIKE', $pattern);
            $data->orWhere('width', 'LIKE', $pattern);
            $data->orWhere('height', 'LIKE', $pattern);
            $data->orWhere('faults.created_at', 'LIKE', $pattern);
            $data->orWhere('faults.text', 'LIKE', $pattern);
        }

        $data = $data->paginate(5);

        //dd($data);

        return view('/comments/index')->with(compact('data', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}



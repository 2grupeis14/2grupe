<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fault extends Model
{
    //
    protected $fillable = [
        'text','user_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function fault_status()
    {
        return $this->hasMany('App\Fault_status');
    }
}

<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->call(DepartmentTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(User_roleTableSeeder::class);

        $this->call(UserTableSeeder::class);
        $this->call(CommentTableSeeder::class);
        $this->call(FaultTableSeeder::class);
        $this->call(Fault_StatusTableSeeder::class);
        $this->call(StatusTableSeeder::class);

    }
}

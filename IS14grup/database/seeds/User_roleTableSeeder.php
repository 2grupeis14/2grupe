<?php

use App\User_role as User_role;
use Illuminate\Database\Seeder;

class User_roleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User_role::truncate();

        User_role::create([
            'role_id' => '12',
            'user_id' => '10',
            'department_id' => '2'
        ]);

        User_role::create([
            'role_id' => '6',
            'user_id' => '8',
            'department_id' => '3'
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

use App\Fault as Fault;
class FaultTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Fault::truncate();

        Fault::create([
            'text' => 'keiciamas maitinimo kabelis' ,
            'user_id' => '1',

        ]);
        Fault::create([
            'text' => 'Nera interneto' ,
            'user_id' => '2',

        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

use App\User as User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {





        User::truncate();

        User::create([
            'name' => 'Tomas' ,
            'surname' => 'Erkenas',
            'email' => 'tomhoc@gmail.com',
            'password' => bcrypt('test'),
            'pn' => '39547865874',
            'address' => 'klaipedos g.18',
            'phone' => '+37068954862'
        ]);

        User::create([
            'name' => 'Jonas' ,
            'surname' => 'Autonas',
            'email' => 'Jonas@gmail.com',
            'password' => bcrypt('test'),
            'pn' => '39547865988',
            'address' => 'klaipedos g.19',
            'phone' => '+37068954458'
        ]);

    }
}

<?php

use Illuminate\Database\Seeder;

use App\Department as Department;

class DepartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Department::truncate();

        Department::create([
            'name' => 'Tomas'
        ]);

        Department::create([
            'name' => 'Jonas'
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

use App\Fault_status as Fault_status;

class Fault_StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Fault_status::truncate();

        Fault_status::create([
            'status_id' => '2',
            'fault_id' =>'1'
        ]);
        Fault_status::create([
            'status_id' => '1',
            'fault_id' =>'2'
        ]);
    }
}

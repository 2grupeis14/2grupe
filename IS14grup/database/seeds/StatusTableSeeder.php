<?php

use Illuminate\Database\Seeder;
use App\Status as Status;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Status::truncate();

        Status::create([
            'name' => 'tvarkomas'
        ]);
        Status::create([
            'name' => 'naujas'
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

use App\Comment as Comment;
class CommentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Comment::truncate();

        Comment::create([
            'text' => 'keiciamas maitinimo kabelis' ,
            'user_id' => '1',
            'fault_id' => '01'

        ]);
        Comment::create([
            'text' => 'Nera interneto' ,
            'user_id' => '2',
            'fault_id' => '02'

        ]);
    }
}

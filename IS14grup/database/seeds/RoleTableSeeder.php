<?php

use App\Role as Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Role::truncate();

        Role::create([
            'name' => 'Tomas'
        ]);

        Role::create([
            'name' => 'Jonas'
        ]);
    }
}

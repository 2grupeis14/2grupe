@extends('layouts.app')

@section('content')

                        <head>

                            <title>Naujas</title>


                        </head>
                        <body>
                        <div class="container-fluid">

                            <nav class="navbar navbar-default">
                                <div class="container-fluid">
                                    <!-- Brand and toggle get grouped for better mobile display -->
                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                                data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                        <a class="navbar-brand" href="#">Sistemos pavadinimas</a>
                                    </div>

                                    <!-- Collect the nav links, forms, and other content for toggling -->
                                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                                    </div><!-- /.navbar-collapse -->
                                </div><!-- /.container-fluid -->
                            </nav>
                            <div class="page-header">

                            </div>
                            <div class="row">
                                <div class="col col-lg-6 col-md-8 col-sm-10 col-xs-12 col-lg-offset-3 col-md-offset-2 col-sm-offset-1">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Sukurti nauja</h3>
                                        </div>
                                        <div class="panel-body">

                                            <form class="form-inline">
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="basic-addon1"></span>
                                                    <input type="text" class="form-control" placeholder="naujas" aria-describedby="basic-addon1">
                                                    </div>
                                            </form>
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1"></span>
                                            <input type="textarea" class="form-control">
                                        </div>

                                        <form action="{{ url('/issue/store') }}" method="post">
                                            {{ csrf_field() }}
                                        <button type="submit" class="btn btn-default" >Sukurti</button>
                                            <a href="{{ url('/') }}"></a>
                                        <a class="btn btn-default" href="/" role="button">Atgal</a>
                                            </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </body>

@endsection

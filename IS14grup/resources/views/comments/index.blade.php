@extends('layouts.app')

@section('content')

                        <body>
                        <div class="container-fluid">



                            <div class="row">
                                <div class="col col-lg-10 col-md-12 col-sm-10 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Problema Nr.1</h3>
                                        </div>
                                        <div class="panel-body">

                                            <form class="form-inline">



                                                <a class="btn btn-default" href="/" role="button">atgal</a>
                                            </form>
                                        </div>

                                    </div>
                                    <table class="table table-bordered">
                                        <tr>


                                            <th>Komentaras</th>
                                            <th>Data</th>
                                            <th>Komentarorius</th>
                                        </tr>

                                        @foreach ($data as $key => $object)

                                        <tr>
                                            <td>{{ $object->text }}</td>
                                            <th>2017-01-12</th>
                                            <th>Tomas</th>
                                        </tr>

                                        @endforeach


                                    </table>

                                    <nav aria-label="Page navigation">
                                        <ul class="pagination">
                                            <li>
                                                <a href="#" aria-label="Previous">
                                                    <span aria-hidden="true">&laquo;</span>
                                                </a>
                                            </li>
                                            <li><a href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li><a href="#">4</a></li>
                                            <li><a href="#">5</a></li>
                                            <li>
                                                <a href="#" aria-label="Next">
                                                    <span aria-hidden="true">&raquo;</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>

                            </div>

                        </div>
                        </body>

                        </html>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

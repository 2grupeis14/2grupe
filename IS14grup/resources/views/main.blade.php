@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
          {{-- <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Sistema</div> --}}

                    <div class="panel-body">

                        </head>
                        <body>
                        <div class="container-fluid">

                            {{--}}<nav class="navbar navbar-default">
                                 <div class="container-fluid">
                                     <!-- Brand and toggle get grouped for better mobile display -->
                                     <div class="navbar-header">
                                          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                                  data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                              <span class="sr-only">Toggle navigation</span>
                                              <span class="icon-bar"></span>
                                              <span class="icon-bar"></span>
                                              <span class="icon-bar"></span>
                                          </button>
                                          <a class="navbar-brand" href="#">Sistemos pavadinimas</a>
                                    </div>

                                    <!-- Collect the nav links, forms, and other content for toggling -->
                                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                        <ul class="nav navbar-nav navbar-right">
                                            <li><a href="#">Atsijungti</a></li>
                                        </ul>
                                    </div><!-- /.navbar-collapse -->
                                </div><!-- /.container-fluid -->
                            </nav>--}}

                            <div class="row">
                                <div class="col col-lg-10 col-md-12 col-sm-10 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Būsena</h3>
                                        </div>
                                        <div class="panel-body">

                                            <form class="form-inline">

                                                <select class="form-control">
                                                    <option>Naujas</option>
                                                    <option>Tvarkomas</option>
                                                    <option>Sutvarkytas</option>
                                                </select>
                                                <select class="form-control">
                                                    <option>Priskirtas</option>
                                                    <option>Nepriskirtas</option>
                                                </select>
                                                <button type="submit" class="btn btn-default">pasirinkti</button>
                                                <a class="btn btn-default" href="Create" role="button">Sukurti nauja problema</a>
                                            </form>
                                        </div>

                                    </div>

                                    <table class="table table-bordered">

                                        <tr>
                                            <th class="hidden-xs">Nr.</th>
                                            <th class="hidden-xs">Skyrius</th>
                                            <th class="hidden-xs">Būsena</th>
                                            <th class="hidden-xs">Problema</th>
                                            <th class="hidden-xs">Pagalbinis</th>
                                        </tr>
                                        <tr>
                                            <td class="hidden-xs">1</td>
                                            <td class="hidden-xs">Technologijų skyrius</td>
                                            <td class="hidden-xs">Naujas</td>
                                            <td class="hidden-xs">Nera interneto</td>
                                            <th class="hidden-xs">
                                                @if(Auth::user()->role_id == 1) <a class="btn btn-default" href="administrator" role="button"><span class="glyphicon glyphicon-user" aria-hidden="true"</span></a> @endif
                                                <a class="btn btn-default" href="edit" role="button"><span class="glyphicon glyphicon-cog" aria-hidden="true"</span></a>
                                                <a class="btn btn-default" href="istorija" role="button"><span class="glyphicon glyphicon-book" aria-hidden="true"</span></a></th>
                                        </tr>
                                        <tr>
                                            <td class="hidden-xs">2</td>
                                            <td class="hidden-xs">Buhalterijos skyrius</td>
                                            <td class="hidden-xs">Tvarkomas</td>
                                            <td class="hidden-xs">keiciamas maitinimo kabelis</td>
                                            <th class="hidden-xs">
                                                <a class="btn btn-default" href="administrator" role="button"><span class="glyphicon glyphicon-user" aria-hidden="true"</span></a>
                                                <a class="btn btn-default" href="edit" role="button"><span class="glyphicon glyphicon-cog" aria-hidden="true"</span></a>
                                                <a class="btn btn-default" href="istorija" role="button"><span class="glyphicon glyphicon-book" aria-hidden="true"</span></a></th>
                                        </tr>
                                        <tr>
                                            <td class="hidden-xs">3</td>
                                            <td class="hidden-xs">Valdybos skyrius</td>
                                            <td class="hidden-xs">Sutvarkytas</td>
                                            <td class="hidden-xs">Pakeista mot plokste</td>
                                            <th class="hidden-xs">
                                                @if(Auth::user()->role_id == 1) <a class="btn btn-default" href="administrator" role="button"><span class="glyphicon glyphicon-user" aria-hidden="true"</span></a> @endif
                                                <a class="btn btn-default" href="edit" role="button"><span class="glyphicon glyphicon-cog" aria-hidden="true"</span></a>
                                                <a class="btn btn-default" href="istorija" role="button"><span class="glyphicon glyphicon-book" aria-hidden="true"</span></a></th>
                                        </tr>

                                       {{-- <tr>
                                            <th class="visible-xs">Nr.</th>
                                            <th class="visible-xs">Skyrius</th>
                                            <th class="visible-xs">Būsena</th>
                                            <th class="visible-xs">Problema</th>
                                            <th class="visible-xs">Pagalbinis</th>
                                        </tr>
                                        <tr>
                                            <td class="visible-xs">1</td>
                                            <td class="visible-xs">Technologijų skyrius</td>
                                            <td class="visible-xs">Naujas</td>
                                            <td class="visible-xs">Nera interneto</td>
                                            <th class="visible-xs"> <a class="btn btn-default" href="administrator" role="button"><span class="glyphicon glyphicon-user" aria-hidden="true"</span></a>
                                                <a class="btn btn-default" href="Redaguoti" role="button"><span class="glyphicon glyphicon-cog" aria-hidden="true"</span></a>
                                                <a class="btn btn-default" href="istorija" role="button"><span class="glyphicon glyphicon-book" aria-hidden="true"</span></a></th>
                                        </tr>
                                        <tr>
                                            <td class="visible-xs">2</td>
                                            <td class="visible-xs">Buhalterijos skyrius</td>
                                            <td class="visible-xs">Tvarkomas</td>
                                            <td class="visible-xs">keiciamas maitinimo kabelis</td>
                                            <th class="visible-xs"> <a class="btn btn-default" href="administrator" role="button"><span class="glyphicon glyphicon-user" aria-hidden="true"</span></a>
                                                <a class="btn btn-default" href="Redaguoti" role="button"><span class="glyphicon glyphicon-cog" aria-hidden="true"</span></a>
                                                <a class="btn btn-default" href="istorija" role="button"><span class="glyphicon glyphicon-book" aria-hidden="true"</span></a></th>
                                        </tr>
                                        <tr>
                                            <td class="visible-xs">3</td>
                                            <td class="visible-xs">Valdybos skyrius</td>
                                            <td class="visible-xs">Sutvarkytas</td>
                                            <td class="visible-xs">Pakeista mot plokste</td>
                                            <th class="visible-xs"> <a class="btn btn-default" href="administrator" role="button"><span class="glyphicon glyphicon-user" aria-hidden="true"</span></a>
                                                <a class="btn btn-default" href="Redaguoti" role="button"><span class="glyphicon glyphicon-cog" aria-hidden="true"</span></a>
                                                <a class="btn btn-default" href="istorija" role="button"><span class="glyphicon glyphicon-book" aria-hidden="true"</span></a></th>
                                        </tr>--}}
                                    </table>
                                    <nav aria-label="Page navigation">
                                        <ul class="pagination">
                                            <li>
                                                <a href="#" aria-label="Previous">
                                                    <span aria-hidden="true">&laquo;</span>
                                                </a>
                                            </li>
                                            <li><a href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li><a href="#">4</a></li>
                                            <li><a href="#">5</a></li>
                                            <li>
                                                <a href="#" aria-label="Next">
                                                    <span aria-hidden="true">&raquo;</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>

                            </div>

                        </div>

                        </body>

                        </html>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection